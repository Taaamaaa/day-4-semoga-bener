import React from "react";
import TextField from "@mui/material/TextField";

const Search = ({ handleSearch }) => {
  const handleInputChange = (event) => {
    const searchTerm = event.target.value;
    handleSearch(searchTerm);
  };

  return (
    <TextField
      label="Search"
      variant="outlined"
      size="small"
      onChange={handleInputChange}
      style={{ width: "300px" }}
    />
  );
};

export default Search;
