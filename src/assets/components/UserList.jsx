import React from "react";

const UserList = ({ userList, handleEdit }) => {
  const handleEditUser = (user) => {
    handleEdit(user);
  };

  return (
    <div className="contentBoxUser">
      <div className="userBox">
        {userList.map((user, index) => (
          <div className="userData" key={index}>
            <div className="userLeft">
              <div>
                <span className="label">Name: {user.name}</span>
              </div>
              <div>
                <span className="label">Address: {user.address}</span>
              </div>
            </div>
            <div className="userRight">
              <span className="label">Hobby: {user.hobby}</span>
              <button
                className="editButton"
                onClick={() => handleEditUser(user)}
              >
                Edit
              </button>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default UserList;
