import Button from "@mui/material/Button";
import React, { useState } from "react";
import AddUserDialog from "../assets/components/AddUserDialog";
import Search from "../assets/components/Search";
import UserList from "../assets/components/UserList";
import "../assets/css/Homepage.css";

export const Homepage = () => {
  const [open, setOpen] = useState(false);
  const [name, setName] = useState("");
  const [address, setAddress] = useState("");
  const [hobby, setHobby] = useState("");
  const [userAdded, setUserAdded] = useState(false);
  const [userList, setUserList] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");
  const [nameError, setNameError] = useState(false);
  const [addressError, setAddressError] = useState(false);
  const [hobbyError, setHobbyError] = useState(false);
  const [editingUser, setEditingUser] = useState(null);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleCancel = () => {
    setOpen(false);
    setName("");
    setAddress("");
    setHobby("");
    clearErrors();
    setEditingUser(null);
  };

  const handleClose = () => {
    setOpen(false);
    clearErrors();
    setEditingUser(null);
  };

  const handleSave = () => {
    if (validateInputs()) {
      const newUser = { name, address, hobby };
      if (editingUser) {
        const updatedUserList = userList.map((user) =>
          user === editingUser ? newUser : user
        );
        setUserList(updatedUserList);
      } else {
        setUserList([...userList, newUser]);
      }
      setOpen(false);
      setUserAdded(true);
      setName("");
      setAddress("");
      setHobby("");
      clearErrors();
      setEditingUser(null);
    }
  };

  const handleEdit = (user) => {
    setEditingUser(user);
    setOpen(true);
    setName(user.name);
    setAddress(user.address);
    setHobby(user.hobby);
  };

  const validateInputs = () => {
    let isValid = true;

    if (name.trim() === "") {
      setNameError(true);
      isValid = false;
    } else {
      setNameError(false);
    }

    if (address.trim() === "") {
      setAddressError(true);
      isValid = false;
    } else {
      setAddressError(false);
    }

    if (hobby.trim() === "") {
      setHobbyError(true);
      isValid = false;
    } else {
      setHobbyError(false);
    }

    return isValid;
  };

  const clearErrors = () => {
    setNameError(false);
    setAddressError(false);
    setHobbyError(false);
  };

  const handleNameChange = (e) => {
    setName(e.target.value);
  };

  const handleAddressChange = (e) => {
    setAddress(e.target.value);
  };

  const handleHobbyChange = (e) => {
    setHobby(e.target.value);
  };

  const handleSearch = (searchTerm) => {
    setSearchTerm(searchTerm);
  };

  const handleDelete = (user) => {
    const updatedUserList = userList.filter((u) => u !== user);
    setUserList(updatedUserList);
  };

  const filteredUserList = userList.filter((user) => {
    const { name, address, hobby } = user;
    const searchTermLower = searchTerm.toLowerCase();
    return (
      name.toLowerCase().includes(searchTermLower) ||
      address.toLowerCase().includes(searchTermLower) ||
      hobby.toLowerCase().includes(searchTermLower)
    );
  });

  return (
    <div className="customContainer">
      <div className="headerContent">
        <div className="titleApp">
          <span className="titleName">My App</span>
        </div>
        <div className="addUser">
          <Button
            variant="contained"
            onClick={handleOpen}
            className="addUserButton"
          >
            Add User
          </Button>
          <AddUserDialog
            open={open}
            handleClose={handleClose}
            handleSave={handleSave}
            handleCancel={handleCancel}
            name={name}
            address={address}
            hobby={hobby}
            handleNameChange={handleNameChange}
            handleAddressChange={handleAddressChange}
            handleHobbyChange={handleHobbyChange}
            nameError={nameError}
            addressError={addressError}
            hobbyError={hobbyError}
          />
        </div>
      </div>
      <div className="bodyContent">
        {userList.length > 0 ? (
          <>
            <div className="searchButton">
              <Search handleSearch={handleSearch} />
            </div>
            <UserList
              userList={filteredUserList}
              handleEdit={handleEdit}
              handleDelete={handleDelete}
            />
          </>
        ) : (
          <div className="contentBox">
            <span className="zero">0</span>
            <span className="User">User</span>
          </div>
        )}
      </div>
    </div>
  );
};

export default Homepage;
